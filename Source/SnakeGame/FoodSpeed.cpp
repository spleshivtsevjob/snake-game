// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSpeed.h"
#include "SnakeBase.h"

// Sets default values
AFoodSpeed::AFoodSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AFoodSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->MovementSpeed = Snake->MovementSpeed / 2;
			Snake->SetActorTickInterval(Snake->MovementSpeed);
			//Snake->AddSnakeElement(2);

		}
	}

}
