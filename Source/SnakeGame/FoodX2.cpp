// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodX2.h"
#include "SnakeBase.h"

// Sets default values
AFoodX2::AFoodX2()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodX2::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodX2::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AFoodX2::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement(2);

		}
	}

}
